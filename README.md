# HashCode 2018 - Extended Round Solution

More info: https://hashcode.withgoogle.com

## About

This was the solution we refined in the following days which much better implements our ideas and put us in the 97th position over 2000 partecipants and 12th in our country.

## Partecipants

* [Alessio Giuseppe Calì](https://www.linkedin.com/in/alessio-giuseppe-cali/)
* [Enrico Panetta](https://www.linkedin.com/in/enrico-panetta/)
* [Jeanpierre Francois](https://www.linkedin.com/in/jeanpierre-francois-7b2613129/)
* [Simone Leonardi](https://www.linkedin.com/in/simone-leonardi-42a6536b/)