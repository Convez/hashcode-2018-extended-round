﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using Accord.MachineLearning;

namespace ConsoleApp1
{
    class Program
    {
        static int rows, columns, veicoli, corse, bonus, maxDistanza;

        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Minchia fai?!!!");
                Console.ReadKey();
                return;
            }
            if (File.Exists(args[0]) == false)
            {
                Console.WriteLine("File " + args[0] + " doesn't exist in " + Directory.GetCurrentDirectory());
                Console.ReadKey();
                return;
            }
            Console.WriteLine("File: " + args[0] + "\n");
            StreamReader input = File.OpenText(args[0]);
            var primariga = input.ReadLine().Split(" ");
            rows = Int32.Parse(primariga[0]);
            columns = Int32.Parse(primariga[1]);
            veicoli = Int32.Parse(primariga[2]);
            corse = Int32.Parse(primariga[3]);
            bonus = Int32.Parse(primariga[4]);
            maxDistanza = Int32.Parse(primariga[5]);
            List<Corsa> listaCorse = new List<Corsa>();
            int idCorse = 0;
            while (!input.EndOfStream)
            {
                var riga = input.ReadLine().Split(" ");
                int row1 = Int32.Parse(riga[0]);
                int col1 = Int32.Parse(riga[1]);
                int row2 = Int32.Parse(riga[2]);
                int col2 = Int32.Parse(riga[3]);
                int earlStart = Int32.Parse(riga[4]);
                int earlFin = Int32.Parse(riga[5]);
                if (row1 < 0 || col1 < 0 || row2 < 0 || col2 < 0 || row1 > rows || row2 > rows || col2 > columns || col1 > columns)
                    continue;
                listaCorse.Add(new Corsa(idCorse++, new Point(row1, col1), new Point(row2, col2), earlStart, earlFin));
            }
            input.Close();
            List<Taxi> taxis = new List<Taxi>();
            for (int i = 0; i < veicoli; i++)
            {
                taxis.Add(new Taxi(i, bonus));
            }


            //var pm = PuntoMedio(taxis);
            //var stats = MediaCorse(listaCorse);
            //double avg = stats.Item1, stdDev = stats.Item2;
            //double threshold = avg + 3 * stdDev;
            //var cluster = (from corsa in listaCorse
            //               where corsa.distanza <= threshold
            //               select corsa).ToList();
            //var outliers = (from corsa in listaCorse
            //                where corsa.distanza > threshold
            //                select corsa).ToList();
            //var cartProductOrdered = (from corsa in listaCorse
            //                         from taxi in taxis
            //                         select new { corsa, taxi } into tmp
            //                         orderby tmp.corsa.DistanzaPartenzaDa(pm) ascending, //ordinamento per distanza dal centro
            //                                 tmp.corsa.distanza descending
            //                         group tmp by tmp.corsa).ToList();
            //Versione 4. Specifica per Metropolis. 
            //Idea: In una metropoli i taxi fanno avanti e indietro in un'area piccola.
            //Quindi ad ogni giro far avvicinare e poi allontanare i taxi dal centro.
            //(Assegnare quindi 2 corse ad ogni iterazione)
            //TODO ^this


            //Versione 3. La migliore fino ad ora
            //Assegna_cluster(listaCorse, taxis);
            //Assegna(cluster, taxis);
            //Assegna(outliers, taxis);

            // Versione 2. Funziona bene sul dataset C
            //foreach (var taxi in taxis)
            //{
            //    while (taxi.DistanzaPercorsa < maxDistanza && listaCorse.Count > 0)
            //    {
            //        var selectedCorsa = (from corsa in listaCorse
            //                             orderby corsa.DistanzaPartenzaDa(taxi.CurrentPosition()) ascending,
            //                                 corsa.distanza ascending
            //                             select corsa).FirstOrDefault();
            //        if (selectedCorsa == null)
            //            break;
            //        taxi.AddCorsa(selectedCorsa);
            //        listaCorse.Remove(selectedCorsa);
            //    }

            //}

            // Versione 1. Funziona benino sui dataset B ed E
            //bool selected = true;
            //while (selected)
            //{
            //    selected = false;
            //    List<Taxi> toAssign = new List<Taxi>(taxis);
            //    cartProductOrdered.ToList().ForEach(corsa =>
            //    {
            //        if (toAssign.Count == 0)
            //            return;
            //        var selectedTaxi = (from taxiList in corsa
            //                            where taxiList.taxi.IsFeasable(taxiList.corsa)
            //                            orderby Math.Abs(taxiList.taxi.ProbabilitaBonus(taxiList.corsa)) ascending
            //                            select taxiList).FirstOrDefault();
            //        if (selectedTaxi != null)
            //        {
            //            selectedTaxi.taxi.AddCorsa(selectedTaxi.corsa);
            //            cartProductOrdered.Remove(corsa);
            //            toAssign.Remove(selectedTaxi.taxi);
            //            selected = true;
            //        }
            //    });
            //    var pMed = PuntoMedio((from ord in cartProductOrdered
            //                           select ord.Key).ToList());
            //    cartProductOrdered = (from ord in cartProductOrdered
            //                          orderby ord.Key.DistanzaPartenzaDa(pMed) descending,
            //                          ord.Key.distanza ascending
            //                          select ord).ToList();

            //}

            Assegna_cluster(listaCorse.ToList(), taxis.ToList(), 45);
            Console.WriteLine();

            //Sezione output
            StreamWriter output = File.CreateText(args[1]);
            taxis.ForEach(x => output.WriteLine(x.ToString()));
            output.Close();
            Console.WriteLine("Total score " + taxis.Select(x => x.Score).Sum());
            Console.ReadKey();
        }
        public static Point PuntoMedioArrivi(List<Corsa> corse)
        {
            int xM = corse.Select(x => x.arrivo.X).Sum() / corse.Count;
            int yM = corse.Select(x => x.arrivo.Y).Sum() / corse.Count;
            return new Point(xM, yM);
        }
        public static Point PuntoMedioPartenze(List<Corsa> corse)
        {
            int xM = corse.Select(x => x.partenza.X).Sum() / corse.Count;
            int yM = corse.Select(x => x.partenza.Y).Sum() / corse.Count;
            return new Point(xM, yM);
        }
        public static Point PuntoMedio(List<Taxi> taxis)
        {
            int xM = taxis.Select(x => x.CurrentPosition().X).Sum() / taxis.Count;
            int yM = taxis.Select(x => x.CurrentPosition().Y).Sum() / taxis.Count;
            return new Point(xM, yM);
        }
        
        public static Tuple<double, double> MediaCorse(List<Corsa> listaCorse) 
        {
            double avg = 0;
            double stdDev = 0;

            avg = listaCorse.Average(corsa => corsa.distanza);
            listaCorse.ForEach(corsa => stdDev += Math.Pow(corsa.distanza - avg, 2));
            stdDev /= listaCorse.Count;
            stdDev = Math.Sqrt(stdDev);

            return Tuple.Create<double, double>(avg, stdDev);
        }

        public static void Assegna(List<Corsa> listaCorse, List<Taxi> taxis) 
        {
            bool selected = true;
            var listTaxi = (from taxi in taxis
                            orderby taxi.DistanzaPercorsa
                            select taxi).ToList();
            while (selected && listaCorse.Count > 0) {
                selected = false;
                foreach (var taxi in listTaxi) {
                    if (taxi.DistanzaPercorsa >= maxDistanza)
                        continue;
                    var selectedCorsa = (from corsa in listaCorse
                                         where taxi.IsFeasable(corsa)
                                         orderby
                                            taxi.ProbabilitaBonus(corsa) ascending,
                                            corsa.DistanzaPartenzaDa(taxi.CurrentPosition()) ascending,
                                            corsa.deadline ascending,
                                            corsa.distanza ascending
                                         select corsa).FirstOrDefault();
                    if (selectedCorsa == null) {
                        selectedCorsa = (from corsa in listaCorse
                                         orderby 
                                            taxi.ProbabilitaBonus(corsa) ascending,
                                            corsa.DistanzaPartenzaDa(taxi.CurrentPosition()) ascending,
                                            corsa.deadline ascending,
                                            corsa.distanza descending
                                         select corsa).FirstOrDefault();
                        if (selectedCorsa == null)
                            continue;
                    }
                    taxi.AddCorsa(selectedCorsa);
                    listaCorse.Remove(selectedCorsa);
                    selected = true;
                }
                listTaxi = (from taxi in taxis
                            orderby taxi.DistanzaPercorsa
                            select taxi).ToList();
            }
        }

        public static void Assegna_cluster(List<Corsa> listaCorse, List<Taxi> taxis, int nClusters)
        {
            bool selected = true;
            var listTaxi = (from taxi in taxis
                            orderby taxi.DistanzaPercorsa
                            select taxi).ToList();

            var clusters = ComputeClusters(listaCorse,nClusters);

            while (selected && listaCorse.Count > 0)
            {
                selected = false;
                foreach (var taxi in listTaxi)
                {
                    if (taxi.DistanzaPercorsa >= maxDistanza)
                        continue;

                    var selectedCorsa = (from corsa in listaCorse
                                            where taxi.IsFeasable(corsa)
                                            orderby
                                            taxi.ProbabilitaBonus(corsa) ascending,
                                            corsa.DistanzaPartenzaDa(taxi.CurrentPosition()) ascending,
                                            MinDistFromClusters(clusters, corsa) ascending,
                                            corsa.deadline ascending,
                                            corsa.distanza ascending
                                            select corsa).FirstOrDefault();
                    if (selectedCorsa == null)
                    {
                        //selectedCorsa = (from corsa in listaCorse
                        //                 orderby
                        //                 taxi.ProbabilitaBonus(corsa) ascending,
                        //                 MinDistFromClusters(clusters, corsa) ascending,
                        //                 corsa.DistanzaPartenzaDa(taxi.CurrentPosition()) ascending,
                        //                 corsa.deadline ascending,
                        //                 corsa.distanza descending
                        //                 select corsa).FirstOrDefault();
                        if (selectedCorsa == null)
                        {
                            continue;
                        }
                    }
                    listaCorse.Remove(selectedCorsa);
                    taxi.AddCorsa(selectedCorsa);
                    selected = true;
                    Console.Write("Assigned rides: {0:0.00}%\r", (1 - ((float)listaCorse.Count) / corse) * 100);
                }
                listTaxi = (from taxi in taxis
                            orderby taxi.DistanzaPercorsa
                            select taxi).ToList();

                if (listaCorse.Count * listaCorse.Count > nClusters)
                    clusters = ComputeClusters(listaCorse, nClusters);
            }
        }

        private static KMeansClusterCollection ComputeClusters(List<Corsa> listaCorse, int nClusters)
        {
            double[][] observations = new double[listaCorse.Count][];
            int i = 0;
            foreach (var corsa in listaCorse)
            {
                observations[i++] = new double[]
                {
                    corsa.partenza.X,
                    corsa.partenza.Y
                };
            }
            i = (int) Math.Min(Math.Sqrt(listaCorse.Count), nClusters);
            //i = 1;
            Accord.Math.Random.Generator.Seed = 0;
            KMeans kmeans = new KMeans(k: (i + (i + 1) % 2)); //k dispari
            var clusters = kmeans.Learn(observations);
            int[] labels = clusters.Decide(observations);
            return clusters;
        }

        private static int MinDistFromClusters(KMeansClusterCollection clusters, Corsa corsa)
        {
            int min = int.MaxValue;//corsa.tempoArrivoMax;
            foreach(var centroid in clusters.Centroids)
            {
                min = Math.Min(
                    min,
                    corsa.DistanzaArrivoDa(
                        new Point( 
                            (int)Math.Round(centroid.First()),  
                            (int)Math.Round((double)centroid.GetValue(1)) 
                        )   
                    )
                );
            }
            return min;
        }
    }
}
